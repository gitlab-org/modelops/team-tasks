# :robot: ModelOps - [Direction](https://about.gitlab.com/direction/modelops/)
Enable and empower data science workloads on GitLab

- ~"devops::modelops" 
  - ~"group::mlops" @gitlab-org/modelops/mlops 
  - ~"group::dataops" 

| Category | Direction |  Maturity | Priority |
| ------ | ------ | ------ | ------ |
| ~"Category:MLOps"  |[MLOps Strategy](https://about.gitlab.com/direction/modelops/mlops/) |  ~"maturity::minimal"  | ~P2  |
| ~"Category:DataOps"  |[DataOps Strategy](https://about.gitlab.com/direction/modelops/dataops/) |  ~"maturity::planned"  | ~P3  |

# Themes

### Theme 1
### Theme 2
### Theme 3


# Outcomes


### Release Post Candidates
https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=opened&label_name[]=release%20post%20item&label_name[]=devops%3A%3Amodelops

### Feedback

* [retrospective issue](TBD)


## Helpful Links :link: 
  * [How we work](https://about.gitlab.com/handbook/engineering/development/modelops/)   
  * Slack channels: 
    * [#data-science-section](https://gitlab.slack.com/archives/C0422RW4UMU)
    * [#s_modelops](https://gitlab.slack.com/archives/C01F1R4J7FW)
    * [#g_mlops](https://gitlab.slack.com/archives/C01ESHPNHS9)
  * Issue boards - overview of all [workflow stages](https://gitlab.com/groups/gitlab-org/modelops/applied-ml/review-recommender/-/boards/4228790)
  * Metrics
    * [Official ModelOps Performance Indicators](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/modelops-section/)
    *

/label ~"Category:MLOps" ~"Planning Issue" ~"devops::modelops" ~"group::applied ml" ~meta ~"section::data-science" 

/cc @gl-quality/data-science-qe  @vincywilson @tauriedavis @katiemacoy @achueshev @a_akgun @srayner @HongtaoYang @bcardoso- @AndrasHerczeg @dbernardi @tle_gitlab @nkhalwadekar

/assign @tmccaslin @mray2020

This issue is generated from [the template located here](https://gitlab.com/gitlab-org/modelops/team-tasks/-/blob/main/.gitlab/issue_templates/planning-issue.md)
