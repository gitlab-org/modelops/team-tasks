# :robot: AI Assisted - [Direction](https://about.gitlab.com/direction/modelops/ai_assisted)
### Enriching GitLab capabilities with Machine Learning (ML) and Artificial Intelligence (AI)

- ~"section::data-science" ~"devops::modelops" 
  - ~"group::ai assisted" @gitlab-org/modelops/applied-ml 
  - ~"Category:Workflow Automation" 
  - ~"Category:Code Suggestions" 
  - ~"Category:Intelligent Code Security" 
- PM: @nkhalwadekar 
- UX: @tauriedavis 
- EM: @mray2020 

| Category | Direction |  Maturity | Priority |
| ------ | ------ | ------ | ------ |
| ~"Category:Code Suggestions"  |[Direction Strategy](https://about.gitlab.com/direction/modelops/ai_assisted/code_suggestions/) |  ~"maturity::minimal"  | ~P1  |
| ~"Category:Workflow Automation"  | [Direction Strategy](https://about.gitlab.com/direction/modelops/ai_assisted/workflow_automation/)| ~"maturity::minimal" | ~P2  |
| ~"Category:Intelligent Code Security"  |[Direction Strategy](https://about.gitlab.com/direction/modelops/ai_assisted/code_suggestions/intelligent_code_security) |  ~"maturity::planned"  | ~P3  |

# Themes

### Theme 1
### Theme 2
### Theme 3


# Outcomes


### Release Post Candidates
https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=opened&label_name[]=release%20post%20item&label_name[]=devops%3A%3Amodelops

### Feedback

* [retrospective issue](TBD)


## Helpful Links :link: 
  * [How we work](https://about.gitlab.com/handbook/engineering/development/data-science/ai-assisted/)   
  * Slack channels: 
    * [#data-science-section](https://gitlab.slack.com/archives/C0422RW4UMU)
    * [#s_modelops](https://gitlab.slack.com/archives/C01F1R4J7FW)
    * [#g_ai_assisted](https://gitlab.slack.com/archives/C023YB2FEUC)
    * [#f_code_suggestions](https://gitlab.slack.com/archives/C048Z2DHWGP)

  * Issue boards - overview of all [workflow stages](https://gitlab.com/groups/gitlab-org/modelops/applied-ml/review-recommender/-/boards/4228790)
  * Metrics
    * [Official ModelOps Performance Indicators](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/modelops-section/)
    *

/label ~"section::data-science" ~"devops::modelops" ~"group::ai assisted" ~"Category:Workflow Automation" ~"Category:Code Suggestions" ~"Category:Intelligent Code Security" ~meta ~"Planning Issue" 

/cc @gl-quality/data-science-qe  @vincywilson @tauriedavis @katiemacoy @achueshev @a_akgun @srayner @HongtaoYang @bcardoso- @AndrasHerczeg @dbernardi @tle_gitlab @tmccaslin

/assign @nkhalwadekar @mray2020

This issue is generated from [the template located here](https://gitlab.com/gitlab-org/modelops/team-tasks/-/blob/main/.gitlab/issue_templates/ai-assisted-milestone-planning.md)

ai-assisted-milestone-planning
